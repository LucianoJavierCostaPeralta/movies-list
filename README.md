# Movies List

## Autor: Luciano Javier Costa Peralta

Este es un proyecto simple de lista de películas que utiliza Vite, React y Tailwind CSS.

### Clonar e Instalar

Para clonar y ejecutar el proyecto localmente, asegúrate de tener Node.js instalado. Luego, sigue estos pasos:

```bash
git clone https://gitlab.com/LucianoJavierCostaPeralta/movies-list
cd movies
npm install
npm run dev
```

[Ir a la Aplicación](https://movies-list-pi.vercel.app)

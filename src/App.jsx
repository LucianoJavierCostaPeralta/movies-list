import { Footer, Header, MoviesList } from "./components";

export default function App() {
  return (
    <>
      <Header />
      <MoviesList />
      <Footer />
    </>
  );
}

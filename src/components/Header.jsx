export const Header = () => {
  return (
    <header
      className="relative text-white flex items-center justify-center "
      style={{
        backgroundImage: `url('/assets/banner.jpg')`,
        backgroundSize: "cover",
        backgroundPosition: "center",
        height: 350,
      }}
    >
      <div className="text-center">
        <h1 className="text-4xl font-bold mb-4 text-gray-300">
          ¡Bienvenido a nuestra app de películas!
        </h1>
        <p className="text-2xl mb-8 text-gray-700">
          Encuentra tus películas favoritas aquí.
        </p>
      </div>
    </header>
  );
};

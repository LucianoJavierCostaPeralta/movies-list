import Data from "../data/data.json";
import { Card } from "./Card";

export const MoviesList = () => {
  return (
    <main className="container mx-auto p-5 h-full">
      {Data?.length > 0 && (
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
          {Data?.map((item, index) => (
            <Card key={index} item={item} />
          ))}
        </div>
      )}
    </main>
  );
};

export const Footer = () => {
  return (
    <footer className="bg-gray-800 text-white">
      <div className="container mx-auto p-5 text-center">
        <p className="text-sm">
          {`© ${new Date().getFullYear()} Tu Aplicación de Películas. Todos los
            derechos reservados.`}
        </p>
      </div>
    </footer>
  );
};

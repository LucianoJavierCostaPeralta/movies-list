export const Card = ({ item }) => {
  const getMetascoreColor = (metascore) => {
    if (metascore >= 0 && metascore <= 49) {
      return "text-red-500";
    } else if (metascore >= 50 && metascore <= 59) {
      return "text-yellow-500";
    } else if (metascore >= 60 && metascore <= 100) {
      return "text-green-500";
    } else {
      return "text-gray-500";
    }
  };
  return (
    <div className="bg-white rounded-lg shadow-lg hover:shadow-xl">
      {item.poster && (
        <img
          src={item.poster}
          alt={item.title}
          className="w-full h-64 object-cover rounded-t-lg"
        />
      )}
      <div className="p-6">
        {item.title && (
          <h2 className="text-xl font-bold text-gray-800 mb-2">{item.title}</h2>
        )}
        {item.releaseDate && (
          <p className="text-gray-600 mb-2">
            Año de Lanzamiento: {item.releaseDate}
          </p>
        )}
        {item.duration && (
          <p className="text-gray-600 mb-2">Duración: {item.duration}</p>
        )}
        {item.maturity && (
          <p className="text-gray-600 mb-2">
            Clasificación de Madurez: {item.maturity}
          </p>
        )}
        {item.genres && (
          <p className="text-gray-600 mb-2">
            Géneros: {item.genres.join(", ")}
          </p>
        )}
        {item.rating && (
          <p className="text-gray-600 mb-2">Rating: {item.rating}</p>
        )}
        {item.metascore && (
          <p className={`font-bold ${getMetascoreColor(item.metascore)}`}>
            Metascore: {item.metascore}
          </p>
        )}
        {item.director && (
          <p className="text-gray-600 mb-2">Director: {item.director}</p>
        )}
        {item.mainActors && (
          <p className="text-gray-600 mb-2">
            Actores Principales: {item.mainActors.join(", ")}
          </p>
        )}
        {item.plot && <p className="text-gray-800 mb-2">{item.plot}</p>}
      </div>
    </div>
  );
};
